use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Vector2D {
    pub x: f32,
    pub y: f32,
}