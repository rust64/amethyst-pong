use amethyst::{
    core::Transform,
    ecs::{Join, Read, ReadExpect, ReadStorage, System, WriteStorage},
    audio::{output::Output, Source},
    assets::AssetStorage,
};

use crate::config::GameConfig;
use crate::components::{ball, paddle};
use crate::user_experience::audio;

pub struct BounceSystem;

impl<'s> System<'s> for BounceSystem {
    type SystemData = (
        WriteStorage<'s, ball::Ball>,
        ReadStorage<'s, paddle::Paddle>,
        ReadStorage<'s, Transform>,
        Read<'s, AssetStorage<Source>>,
        ReadExpect<'s, audio::Sounds>,
        Option<Read<'s, Output>>,
        Read<'s, GameConfig>,
    );

    fn run(&mut self, (mut balls, paddles, transforms, storage, sounds, audio_output, game_config): Self::SystemData) {
        // Check whether a ball collided, and bounce off accordingly.
        //
        // We also check for the velocity of the ball every time, to prevent multiple collisions
        // from occurring.
        for (ball, transform) in (&mut balls, &transforms).join() {
            let ball_x = transform.translation().x;
            let ball_y = transform.translation().y;

            // Bounce at the top or the bottom of the arena.
            if (ball_y <= ball.radius && ball.velocity.y < 0.0)
                || (ball_y >= game_config.arena.height - ball.radius && ball.velocity.y > 0.0)
            {
                ball.velocity.y = -ball.velocity.y;
                audio::play_bounce_sound(&*sounds, &storage, audio_output.as_deref());
            }

            // Bounce at the paddles.
            for (paddle, paddle_transform) in (&paddles, &transforms).join() {
                let paddle_x = paddle_transform.translation().x - (paddle.width * 0.5);
                let paddle_y = paddle_transform.translation().y - (paddle.height * 0.5);

                // To determine whether the ball has collided with a paddle, we create a larger
                // rectangle around the current one, by subtracting the ball radius from the
                // lowest coordinates, and adding the ball radius to the highest ones. The ball
                // is then within the paddle if its center is within the larger wrapper
                // rectangle.
                if point_in_rect(
                    ball_x,
                    ball_y,
                    paddle_x - ball.radius,
                    paddle_y - ball.radius,
                    paddle_x + paddle.width + ball.radius,
                    paddle_y + paddle.height + ball.radius,
                ) {
                    if (paddle.side == paddle::Side::Left && ball.velocity.x < 0.0)
                        || (paddle.side == paddle::Side::Right && ball.velocity.x > 0.0)
                    {
                        ball.velocity.x = -ball.velocity.x;
                        audio::play_bounce_sound(&*sounds, &storage, audio_output.as_deref());
                    }
                }
            }
        }
    }
}

// A point is in a box when its coordinates are smaller or equal than the top
// right and larger or equal than the bottom left.
fn point_in_rect(x: f32, y: f32, left: f32, bottom: f32, right: f32, top: f32) -> bool {
    x >= left && x <= right && y >= bottom && y <= top
}