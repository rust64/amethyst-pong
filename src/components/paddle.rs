use amethyst::{
    assets::Handle,
    core::transform::Transform,
    ecs::{Component, DenseVecStorage},
    prelude::*,
    renderer::{SpriteRender, SpriteSheet},
};

use crate::config::GameConfig;

pub const PADDLE_HEIGHT: f32 = 16.0;
pub const PADDLE_WIDTH: f32 = 4.0;

#[derive(PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Paddle {
    fn new(side: Side) -> Paddle {
        Paddle {
            side,
            width: PADDLE_WIDTH,
            height: PADDLE_HEIGHT,
        }
    }
}

// By implementing Component for the Paddle struct, it can now be attached to entities in the game.
impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}

/// Initialises one paddle on the left, and one paddle on the right.
pub fn initialise_paddles(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    // Assign the sprites for the paddles
    let sprite_render = SpriteRender::new(sprite_sheet_handle, 0);  // paddle is the first sprite in the sprite_sheet
    initialise_paddle(world, sprite_render.clone(), Side::Left);
    initialise_paddle(world, sprite_render, Side::Right);
}

fn initialise_paddle(world: &mut World, sprite_render: SpriteRender, side: Side) {
    let mut transform = Transform::default();

    // Correctly position the paddle.
    let config = world.read_resource::<GameConfig>();
    transform.set_translation_xyz(
        match side {
            Side::Left => PADDLE_WIDTH * 0.5,
            Side::Right => config.arena.width - PADDLE_WIDTH * 0.5,
        },
        config.arena.height / 2.0,
        0.0,
    );
    drop(config);

    // Create a plank entity.
    world
        .create_entity()
        .with(sprite_render)
        .with(Paddle::new(side))
        .with(transform)
        .build();
}
