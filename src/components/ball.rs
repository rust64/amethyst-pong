use amethyst::{
    assets::Handle,
    core::{
        transform::Transform,
    },
    ecs::{Component, DenseVecStorage},
    prelude::*,
    renderer::{SpriteRender, SpriteSheet},
};
use serde::{Deserialize, Serialize};
use crate::utils::geometry::Vector2D;
use crate::config::GameConfig;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Ball {
    pub radius: f32,
    pub velocity: Vector2D,
}

impl Default for Ball {
    fn default() -> Self {
        Ball {
            radius: 2.0,
            velocity: Vector2D {
                x: 50.0,
                y: 50.0,
            }
        }
    }
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}

/// Initialises one ball in the middle-ish of the arena.
pub fn initialise_ball(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let ball = world.read_resource::<GameConfig>().ball.clone();
    // Create the translation.
    let mut local_transform = Transform::default();

    let config = world.read_resource::<GameConfig>();
    local_transform.set_translation_xyz(config.arena.width / 2.0, config.arena.height / 2.0, 0.0);
    drop(config);
    // Assign the sprite for the ball. The ball is the second sprite in the sheet.
    let sprite_render = SpriteRender::new(sprite_sheet_handle, 1);

    world
        .create_entity()
        .with(sprite_render)
        .with(ball)
        .with(local_transform)
        .build();
}