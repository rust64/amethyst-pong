use amethyst::{
    core::transform::Transform,
    prelude::{World, WorldExt, Builder},
    renderer::Camera,
};

use crate::config::GameConfig;

pub fn initialise_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    
    let config = world.read_resource::<GameConfig>();
    transform.set_translation_xyz(config.arena.width * 0.5, config.arena.height * 0.5, 1.0);
    let camera2d = Camera::standard_2d(config.arena.width, config.arena.height);
    drop(config);

    world
        .create_entity()
        .with(camera2d)
        .with(transform)
        .build();
}

