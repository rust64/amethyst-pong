// Keep global variables that user/dev may want to easily change in RON files in config/.
// Do not keep them in src/ as they will force recompiling when modified.

use amethyst::{
    input::{Bindings, StringBindings},
    window::DisplayConfig,
};
use ron::error::SpannedError;
use serde::{Deserialize, Serialize};
use std::{
    fs,
    path::PathBuf,
};
use crate::components::ball;

fn load<T>(path: PathBuf) -> Result<T, SpannedError> where for<'a> T: Deserialize<'a> {
    let file_content = fs::read_to_string(path)?;
    ron::from_str(&file_content)
}

pub struct EngineConfig {
    pub bindings: Bindings<StringBindings>,
    pub display: DisplayConfig,
}

impl EngineConfig {
    pub fn new(root_path: PathBuf) -> Result<EngineConfig, SpannedError> {
        let bindings_path = root_path.join("bindings.ron");
        let display_config_path = root_path.join("display.ron");

        Ok(EngineConfig {
            bindings: load::<Bindings<StringBindings>>(bindings_path)?,
            display: load::<DisplayConfig>(display_config_path)?
        })
    }
}

#[derive(Default)]
pub struct GameConfig {
    pub arena: Arena,
    pub ball: ball::Ball,
}

impl GameConfig {
    pub fn new(root_path: PathBuf) -> Result<GameConfig, SpannedError> {
        Ok(GameConfig {
            arena: load::<Arena>(root_path.join("arena.ron"))?,
            ball: load::<ball::Ball>(root_path.join("ball.ron"))?,
        })
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Arena {
    pub width: f32,
    pub height: f32,
}

impl Default for Arena {
    fn default() -> Self {
        Arena {
            width: 200.0,
            height: 150.0,
        }
    }
}