use amethyst::{
    assets::Handle,
    core:: timing::Time,
    prelude::*,
    renderer::SpriteSheet,
};

use crate::components::{ball, paddle};
use crate::user_experience::{assets, audio, camera, ui};

// core game struct
#[derive(Default)]
pub struct State {
    ball_spawn_timer: Option<f32>,
    sprite_sheet_handle: Option<Handle<SpriteSheet>>,
}

// Trait used by Amethyst's state machine to start, stop, and update the game.
// Its behavior mostly cares about handling the exit signal cleanly, by just quitting the application directly from the current state.
impl SimpleState for State {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) -> () {
        let world = data.world;

        // Wait one second before spawning the ball.
        self.ball_spawn_timer = Some(1.0);

        // Load the spritesheet necessary to render the graphics.
        // `spritesheet` is the layout of the sprites on the image;
        // `texture` is the pixel data.
        self.sprite_sheet_handle = Some(assets::load_sprite_sheet(world));

        // world.register::<Paddle>(); // only needed if no system uses the component
        // world.register::<components::ball::Ball>();

        // we clone because initialise_paddles and initialise_ball consume the handle
        // we use unwrap even though it can panic because the on_start function does not allow us to return a Result
        paddle::initialise_paddles(world, self.sprite_sheet_handle.clone().unwrap());
        camera::initialise_camera(world);
        ui::initialise_scoreboard(world);
        audio::initialise_audio(world);
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if let Some(mut timer) = self.ball_spawn_timer.take() {
            // If the timer isn't expired yet, subtract the time that passed since the last update.
            {
                let time = data.world.fetch::<Time>();
                timer -= time.delta_seconds();
            }
            if timer <= 0.0 {
                // When timer expire, spawn the ball
                ball::initialise_ball(data.world, self.sprite_sheet_handle.clone().unwrap());
            } else {
                // If timer is not expired yet, put it back onto the state.
                self.ball_spawn_timer = Some(timer);
            }
        }
        Trans::None
    }
}
