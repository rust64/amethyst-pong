# Config
It is better to store the config in files than directly in the code to avoid having to recompile everything when we change it.
`.ron` stands for [Rusty Object Notation](https://github.com/ron-rs/ron).

# Bundle & Plugins
A bundle is a collection of systems that, in combination, will provide a certain feature to the engine.
We can write our own bundles.

A system is nothing more than a function that runs once each frame and potentially makes some changes to components. ~ MonoBehaviour in Unity, but a system describes the behavior of all components of a specific type e.g. all enemies.

